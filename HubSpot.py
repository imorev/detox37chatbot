#https://github.com/jpetrucciani/hubspot3
# Если проблемы с SSL:
# pip install --upgrade certifi
# open /Applications/Python\ 3.7/Install\ Certificates.command
# API DOC https://buildmedia.readthedocs.org/media/pdf/hubspot3/latest/hubspot3.pdf

from hubspot3.companies import CompaniesClient
from hubspot3.deals import DealsClient

import json


import configparser


path = "configuration.ini"
config = configparser.ConfigParser()
config.read(path)
API_KEY = str(config.get("DEFAULT", "hubspot_api_key"))  # hubspot api key

client = CompaniesClient(api_key=API_KEY)

print('Print list of companies:')

for company in client.get_all():
    print(company)

deal_id = "1982732761"
deals_client = DealsClient(api_key=API_KEY)

params = {
    "includePropertyVersions": "true"
}  # Note values are camelCase as they appear in the Hubspot Documentation!

deal_data = deals_client.get(deal_id, params=params)
print('Print data of deal')
print(json.dumps(deal_data))


import requests
import json

url= 'https://api.hubapi.com/deals/v1/deal?hapikey=' + API_KEY
headers={}
headers["Content-Type"]="application/json"
data = json.dumps({
  "associations": {
    "associatedCompanyIds": [
      8954037
    ],
    "associatedVids": [
      27136
    ]
  },
  "properties": [
    {
      "value": "Tim's Newer Deal",
      "name": "dealname"
    },
    {
      "value": "appointmentscheduled",
      "name": "dealstage"
    },
    {
      "value": "default",
      "name": "pipeline"
    },
    {
      "value": "101",
      "name": "hubspot_owner_id"
    },
    {
      "value": 1409443200000,
      "name": "closedate"
    },
    {
      "value": "60000",
      "name": "amount"
    }
  ]
})

r=requests.post(url, headers = headers, data = data)

print(r.status_code)
