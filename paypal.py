#https://github.com/paypal/Checkout-Python-SDK
#https://developer.paypal.com/docs/api/overview/#get-credentials§
from paypalcheckoutsdk.core import PayPalHttpClient, SandboxEnvironment


# Creating Access Token for Sandbox Sandbox account sb-5jfad1706335@personal.example.com
#
client_id = "AZLviqN4k5jv5dxFDxaoiJ8v8V3if5BO3S-QOyuFjOX0lqlaBEYgVC4byx4tePKqjcRaIhgXTQsLYZ6L" #Sandbox
client_secret = "EDmUZSPzWa-EYyeBLZtbeaCVCmE_FQKcyhzw-X0KMwhUU5C1-_GjmG_I6aCpuGYl26kY2wbTjf5HtUTh" #Sandbox
# Creating an environment
environment = SandboxEnvironment(client_id=client_id, client_secret=client_secret)
client = PayPalHttpClient(environment)

from paypalcheckoutsdk.orders import OrdersCreateRequest
from paypalhttp import HttpError
# Construct a request object and set desired parameters
# Here, OrdersCreateRequest() creates a POST request to /v2/checkout/orders
request = OrdersCreateRequest()

request.prefer('return=representation')

request.request_body (
  {
    "intent": "CAPTURE",
    "application_context": {
      "brand_name": "EXAMPLE INC",
      "landing_page": "BILLING",
      "shipping_preference": "SET_PROVIDED_ADDRESS",
      "user_action": "CONTINUE"
    },
    "purchase_units": [
      {
        "reference_id": "PUHF",
        "description": "Sporting Goods",

        "custom_id": "CUST-HighFashions",
        "soft_descriptor": "HighFashions",
        "amount": {
          "currency_code": "USD",
          "value": "230.00",
          "breakdown": {
            "item_total": {
              "currency_code": "USD",
              "value": "180.00"
            },
            "shipping": {
              "currency_code": "USD",
              "value": "30.00"
            },
            "handling": {
              "currency_code": "USD",
              "value": "10.00"
            },
            "tax_total": {
              "currency_code": "USD",
              "value": "20.00"
            },
            "shipping_discount": {
              "currency_code": "USD",
              "value": "10"
            }
          }
        },
        "items": [
          {
            "name": "T-Shirt",
            "description": "Green XL",
            "sku": "sku01",
            "unit_amount": {
              "currency_code": "USD",
              "value": "90.00"
            },
            "tax": {
              "currency_code": "USD",
              "value": "10.00"
            },
            "quantity": "1",
            "category": "PHYSICAL_GOODS"
          },
          {
            "name": "Shoes",
            "description": "Running, Size 10.5",
            "sku": "sku02",
            "unit_amount": {
              "currency_code": "USD",
              "value": "45.00"
            },
            "tax": {
              "currency_code": "USD",
              "value": "5.00"
            },
            "quantity": "2",
            "category": "PHYSICAL_GOODS"
          }
        ],
        "shipping": {
          "method": "United States Postal Service",
          "address": {
            "name": {
              "full_name":"John",
              "surname":"Doe"
            },
            "address_line_1": "123 Townsend St",
            "address_line_2": "Floor 6",
            "admin_area_2": "San Francisco",
            "admin_area_1": "CA",
            "postal_code": "94107",
            "country_code": "US"
          }
        }
      }
    ]
  }
)

try:
    # Call API with your client and get a response for your call
    response = client.execute(request)
    print ('Order With Complete Payload:')
    print ('Status Code:', response.status_code)
    print ('Status:', response.result.status)
    print ('Order ID:', response.result.id)
    print ('Intent:', response.result.intent)
    print ('Links:')
    for link in response.result.links:
        print('\t{}: {}\tCall Type: {}'.format(link.rel, link.href, link.method))
        print('Total Amount: {} {}'.format(response.result.purchase_units[0].amount.currency_code, response.result.purchase_units[0].amount.value))
        # If call returns body in response, you can get the deserialized version from the result attribute of the response
        #order = response.result
        #print(order)
except:
  print(Exception.__context__)



